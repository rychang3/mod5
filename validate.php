<?php
session_start();

require 'database.php';


if(isset($_POST['username']) && isset($_POST['password'])) {
	$user = $_POST['username'];
	$pass = $_POST['password'];
	//use php password_hash function to return one way hashed string of pass
	$hash = md5($pass);
	$stmt = $mysqli->prepare("SELECT id, username, password FROM users WHERE username=?");
	$stmt->bind_param('s', $user);

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($id_q, $user_q, $pass_q);
	$stmt->fetch(); //stores the values from the next row to count_q user_q and pass_q
	$hp = md5($pass);

	//ensure count is 1; there is only one row returned 
	if(md5($pass) == $pass_q){
		$_SESSION['user'] = $user_q;
		$_SESSION['userid'] = $id_q;
		$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
		echo "Y";
		exit;

	}else{
		echo "N";
	}
}else{
	echo "N";
}

?>
