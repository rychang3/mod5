TO DO:
	Table Format. How do we add cells in the appropriate row?
	Move script to seperate .js file

<!DOCTYPE html>

<html>

<head>
	<title> Calendar </title>
	<script src= "http://classes.engineering.wustl.edu/cse330/content/calendar.js " > </script>
</head>

<body>

	<div id = "container">

		<div id = "header">
			<span id = "title"> MONTH, YEAR </span> //will have to change this to be dynamic
		</div>

		<div id = "dates">
			<table id = "table">
				<tbody>
					<tr>
					</tr>
				</tbody>
		</div>

	</div>

	<form>
		<input type="submit" name="next month" value="next month" id="next_month_button_hello">
		<br>
		<input type="submit" name="previous month" value="previous month" id="previous_month_button_hello" >
		<br>
	</form>

	<form id = "credentials">

		Username: <br>
		<input type="text"  name="username">
		<br>
		Password: <br>
		<input type="password" name="password">
		<br>
		<input type="submit" value="Login">
		<br><br>
		New user? <a class = "btn" href = "new_user.php"> Click here to sign up! </a>
		<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />

	</form>

</body>

<script type="text/javascript">

//first run - pass in newDate (currentMonth.year, currentMonth.month, 1);

window.onload = function createCalendar(Date d)	//this needs to take in a date object with an empty constructor
{
	var date1 = new Date (d.year, d.month, 1);	//new date of first day of this month
	var day1 = date1.getDay();	//day code 0-6 of the first 
	var month = d.month;	//this month
	var numDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var days = ["S", "M", "T", "W", "T", "F", "S"];
	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August",
	"September", "October", "November", "December"];	//to use in header
	var numRows = Math.ceil(numDays[month]/7); 	//gives the number of rows to appear in the calendar
	var emptyCells = day1;	//counter for empty cells that still need to be placed in the beginning
	var day = 1; //counter for date

	for (var row = 0; row < numRows; row++) { 
		var r = document.createElement('tr');	//make a new row
		for (var column = 0; column < 7; column++){
			if (row ==0){
				var cell = document.createElement("th");
				var label = document.createTextNode(days[column]); 	//add the days headers
				cell.appendChild(label);
				r.appendChild(cell);
			}
			if (row == 1 && emptyCells > 0) {	//must add empty cells so that the first begins on the correct day
				var cell = document.createElement("td");
				r.appendChild(cell);
				emptyCells--;
			}
			else {
				if (day <= numDays[month]){
					var cell = document.createElement("td");
					var label = document.createTextNode(day);
					cell.appendChild(label);
					r.appendChild(cell);
					day++;
				} else {	//create empty cells for the remainder of the last week
					var cell = document.createElement("td");
					r.appendChild(cell);
				}
			}
		}
	var element = document.getElementById("table"); //get calendar
	element.appendChild(r); //add the row to calendar
	}
} //end of function



//////END OF OUR CODE////////


// For our purposes, we can keep the current month in a variable in the global scope
// var currentMonth = new Month(2015, 3); // October 2012

// Change the month when the "next" button is pressed
document.getElementById("next_month_button_hello").addEventListener("click", function(event){
	currentMonth = currentMonth.nextMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
	alert("The new month is "+currentMonth.month+" "+currentMonth.year);
}, false);

document.getElementById("previous_month_button_hello").addEventListener("click", function(event){
	currentMonth = currentMonth.prevMonth(); // Previous month would be currentMonth.prevMonth()
	updateCalendar(); // Whenever the month is updated, we'll need to re-render the calendar in HTML
	alert("The new month is "+currentMonth.month+" "+currentMonth.year);
}, false); 

// This updateCalendar() function only alerts the dates in the currently specified month.  You need to write
// it to modify the DOM (optionally using jQuery) to display the days and weeks in the current month.
function updateCalendar(){
	var weeks = currentMonth.getWeeks();

	for(var w in weeks){
		var days = weeks[w].getDates();
		// days contains normal JavaScript Date objects.

		alert("Week starting on "+days[0]);

		for(var d in days){
			// You can see console.log() output in your JavaScript debugging tool, like Firebug,
			// WebWit Inspector, or Dragonfly.
			console.log(days[d].toISOString());
		}
	}
}



</script>










 <!-- var currentMonth = new Month(2015, 3); 
 document.getElementById("next_month_btn").addEventListener("click", function(event){
 	currentMonth = currentMonth.nextMonth(); 
 	updateCalendar(); 
 	alert("The new month is "+currentMonth.month+" "+currentMonth.year);
 }, false);

 document.getElementById("previous_month_btn").addEventListener("click", function(event){
 	currentMonth = currentMonth.prevMonth(); 
 	updateCalendar(); 
 	alert("The new month is "+currentMonth.month+" "+currentMonth.year);
 }, false);

 function updateCalendar(){
 	var weeks = currentMonth.getWeeks();
 
	for(var w in weeks){
 		var days = weeks[w].getDates();
 		 days contains normal JavaScript Date objects.
 
 		alert("Week starting on "+days[0]);
 
 		for(var d in days){
 			 You can see console.log() output in your JavaScript debugging tool, like Firebug,
 			 WebWit Inspector, or Dragonfly.
 			console.log(days[d].toISOString());
 		}
 	}
 } -->

<!-- // // function nextMonth(){

// // }

// // function prevMonth(){

// // }


// function Week(initial_d) {
// 	"use strict";

// 	this.sunday = initial_d.getSunday();
		
	
// 	this.nextWeek = function () {
// 		return new Week(this.sunday.deltaDays(7));
// 	};
	
// 	this.prevWeek = function () {
// 		return new Week(this.sunday.deltaDays(-7));
// 	};
	
// 	this.contains = function (d) {
// 		return (this.sunday.valueOf() === d.getSunday().valueOf());
// 	};
	
// 	this.getDates = function () {
// 		var dates = [];
// 		for(var i=0; i<7; i++){
// 			dates.push(this.sunday.deltaDays(i));
// 		}
// 		return dates;
// 	};
// }

// function Month(year, month) {
// 	"use strict";
	
// 	this.year = year;
// 	this.month = month;
	
// 	this.nextMonth = function () {
// 		return new Month( year + Math.floor((month+1)/12), (month+1) % 12);
// 	};
	
// 	this.prevMonth = function () {
// 		return new Month( year + Math.floor((month-1)/12), (month+11) % 12);
// 	};
	
// 	this.getDateObject = function(d) {
// 		return new Date(this.year, this.month, d);
// 	};
	
// 	this.getWeeks = function () {
// 		var firstDay = this.getDateObject(1);
// 		var lastDay = this.nextMonth().getDateObject(0);
		
// 		var weeks = [];
// 		var currweek = new Week(firstDay);
// 		weeks.push(currweek);
// 		while(!currweek.contains(lastDay)){
// 			currweek = currweek.nextWeek();
// 			weeks.push(currweek);
// 		}
		
// 		return weeks;
// 	};
// }


-->
